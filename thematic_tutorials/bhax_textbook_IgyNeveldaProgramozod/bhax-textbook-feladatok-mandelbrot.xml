<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Mandelbrot!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section xml:id="bhax-textbook-feladatok-mandelbrot.Mandelbrot">
        <title>A Mandelbrot halmaz</title>
        <para>
            Írj olyan C programot, amely kiszámolja a Mandelbrot halmazt!     
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/hkY9feZbQw8">https://youtu.be/hkY9feZbQw8</link>
        </para>
        <para>
            Megoldás forrása:                
 <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/attention_raising/CUDA/mandelpngt.c++">
                https://gitlab.com/ak_k/bhax/-/blob/master/attention_raising/CUDA/mandelpngt.c++
            </link>          
        </para>
                <figure>
            <title>A Mandelbrot halmaz a komplex síkon</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/mandel.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>A Mandelbrot halmaz a komplex síkon</phrase>
                </textobject>
            </mediaobject>
        </figure>
        <para>
            A Mandelbrot halmazt 1980-ban találta meg Benoit Mandelbrot a 
            komplex számsíkon. Komplex számok azok a számok, amelyek körében 
            válaszolni lehet az olyan egyébként értelmezhetetlen kérdésekre, 
            hogy melyik az a két szám, amelyet összeszorozva -9-et kapunk, 
            mert ez a szám például a 3i komplex szám.
        </para>
        <para>             
            A Mandelbrot halmazt úgy láthatjuk meg, hogy a sík origója középpontú 4 
            oldalhosszúságú négyzetbe lefektetünk egy, mondjuk 800x800-as 
            rácsot és kiszámoljuk, hogy a rács pontjai mely komplex számoknak 
            felelnek meg. A rács minden pontját megvizsgáljuk a 
            z<subscript>n+1</subscript>=z<subscript>n</subscript>
            <superscript>2</superscript>+c, 
            (0&lt;=n) képlet alapján úgy, hogy a c az éppen vizsgált rácspont. 
            A z0 az origó. Alkalmazva a képletet a 
        </para>
        <itemizedlist>
            <listitem>
                <para>
                    z<subscript>0</subscript> = 0
                </para>
            </listitem>
            <listitem>
                <para>
                    z<subscript>1</subscript> = 0<superscript>2</superscript>+c = c
                </para>
            </listitem>
            <listitem>
                <para>
                    z<subscript>2</subscript> = c<superscript>2</superscript>+c
                </para>
            </listitem>
            <listitem>
                <para>
                    z<subscript>3</subscript> = (c<superscript>2</superscript>+c)<superscript>2</superscript>+c
                </para>
            </listitem>
            <listitem>
                <para>
                    z<subscript>4</subscript> = ((c<superscript>2</superscript>+c)<superscript>2</superscript>+c)<superscript>2</superscript>+c
                </para>
            </listitem>
            <listitem>
                <para>
                    ... s így tovább.
                </para>
            </listitem>
        </itemizedlist>
        <para>
            Azaz kiindulunk az origóból (z<subscript>0</subscript>) 
            és elugrunk a rács első pontjába a z<subscript>1</subscript> = c-be, 
            aztán a c-től függően a további z-kbe. Ha ez az utazás kivezet a 
            2 sugarú körből, akkor azt mondjuk, hogy az a vizsgált rácspont 
            nem a Mandelbrot halmaz eleme. Nyilván nem tudunk végtelen sok 
            z-t megvizsgálni, ezért csak véges sok z elemet nézünk meg minden 
            rácsponthoz. Ha eközben nem lép ki a körből, akkor feketére 
            színezzük, hogy az a c rácspont a halmaz része. (Színes meg úgy 
            lesz a kép, hogy változatosan színezzük, például minél későbbi 
            z-nél lép ki a körből, annál sötétebbre). 
        </para>       
        <para>
            A main elején van egy ellenőrzés, ami arról bizonyosodik meg, hogy jól próbáltuk meg futtatni a programot.
            Ha nem, akkor kiírja a terminálra a helyes használatot.
            Ha jól futtatuk a programot, akkor ezt nyílván nem látjuk.
            <programlisting language="c">
<![CDATA[if (argc != 2) {
std::cout << "Hasznalat: ./mandel fajlnev";
return -1;
}]]>
            </programlisting>
        </para>
        <para>
            Megadjuk az értékkészletet, valamint az értelmezési tartományt.
            <programlisting language="c">
<![CDATA[double a = -2.0, b = .7,  c = -1.35, d = 1.35;
int szelesseg = 600, magassag = 600, iteraciosHatar = 1000;]]>
            </programlisting>
        </para>
        <para>
            Létrehozunk egy png-t, amin majd a halmazt fogjuk ábrázolni.
            <programlisting language="c">
<![CDATA[png::image <png::rgb_pixel> kep (szelesseg, magassag);]]>
            </programlisting>
        </para>
        <para>
            A két, egymásba ágyazott ciklus végigmegy a képen / rácson.
            Közben az értelmezési tartományon is végigmegyünk, és megadjuk a 'c' számot, amihez az adott 'z' értéket kiszámoljuk.
            A <function>while</function> ciklus addig megy, amíg a 'z' négyzete kisebb, mint 4.
            Ha elérjük az iterációs határtm akkor az adott 'c' eleme a Madnelbrot halmaznak.
            Megadjuk a pixelek színék, majd lementjük a képet a megadott fájlba.
            <programlisting language="c">
<![CDATA[for (int j=0; j<magassag; ++j) 
{    
    for (int k=0; k<szelesseg; ++k) 
    {
        reC = a+k*dx;
        imC = d-j*dy;
        reZ = 0;
        imZ = 0;
        iteracio = 0;
        while (reZ*reZ + imZ*imZ < 4 && iteracio < iteraciosHatar) 
        {
            ujreZ = reZ*reZ - imZ*imZ + reC;
            ujimZ = 2*reZ*imZ + imC;
            reZ = ujreZ;
            imZ = ujimZ;
            ++iteracio;
        }
        kep.set_pixel(k, j, png::rgb_pixel(255-iteracio%256, 255-iteracio%256, 255-iteracio%256));
    }
    std::cout << "." << std::flush;
}]]>
            </programlisting>
        </para>
    </section>        
        
    <section>
        <title>A Mandelbrot halmaz a <filename>std::complex</filename> osztállyal</title>
        <para>
            Írj olyan C++ programot, amely kiszámolja a Mandelbrot halmazt!                     
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/uRhiRHMqTmM">https://youtu.be/uRhiRHMqTmM</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/attention_raising/Mandelbrot/3.1.2.cpp">https://gitlab.com/ak_k/bhax/-/blob/master/attention_raising/Mandelbrot/3.1.2.cpp</link>            
        </para>
        <para>        
A <link xlink:href="#bhax-textbook-feladatok-mandelbrot.Mandelbrot">Mandelbrot halmaz</link> pontban vázolt
ismert algoritmust valósítja meg a repó <link xlink:href="../../../bhax/attention_raising/Mandelbrot/3.1.2.cpp">
                <filename>bhax/attention_raising/Mandelbrot/3.1.2.cpp</filename>
            </link> nevű állománya.
        </para>
        
        <programlisting language="c++">
<![CDATA[// Verzio: 3.1.2.cpp
// Forditas:
// g++ 3.1.2.cpp -lpng -O3 -o 3.1.2
// Futtatas:
// ./3.1.2 mandel.png 1920 1080 2040 -0.01947381057309366392260585598705802112818 -0.0194738105725413418456426484226540196687 0.7985057569338268601555341774655971676111 0.798505756934379196110285192844457924366
// ./3.1.2 mandel.png 1920 1080 1020 0.4127655418209589255340574709407519549131 0.4127655418245818053080142817634623497725 0.2135387051768746491386963270997512154281 0.2135387051804975289126531379224616102874
// Nyomtatas:
// a2ps 3.1.2.cpp -o 3.1.2.cpp.pdf -1 --line-numbers=1  --left-footer="BATF41 HAXOR STR34M" --right-footer="https://bhaxor.blog.hu/" --pro=color
// ps2pdf 3.1.2.cpp.pdf 3.1.2.cpp.pdf.pdf
//
//
// Copyright (C) 2019
// Norbert Bátfai, batfai.norbert@inf.unideb.hu
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#include <iostream>
#include "png++/png.hpp"
#include <complex>

int
main ( int argc, char *argv[] )
{

  int szelesseg = 1920;
  int magassag = 1080;
  int iteraciosHatar = 255;
  double a = -1.9;
  double b = 0.7;
  double c = -1.3;
  double d = 1.3;

  if ( argc == 9 )
    {
      szelesseg = atoi ( argv[2] );
      magassag =  atoi ( argv[3] );
      iteraciosHatar =  atoi ( argv[4] );
      a = atof ( argv[5] );
      b = atof ( argv[6] );
      c = atof ( argv[7] );
      d = atof ( argv[8] );
    }
  else
    {
      std::cout << "Hasznalat: ./3.1.2 fajlnev szelesseg magassag n a b c d" << std::endl;
      return -1;
    }

  png::image < png::rgb_pixel > kep ( szelesseg, magassag );

  double dx = ( b - a ) / szelesseg;
  double dy = ( d - c ) / magassag;
  double reC, imC, reZ, imZ;
  int iteracio = 0;

  std::cout << "Szamitas\n";

  // j megy a sorokon
  for ( int j = 0; j < magassag; ++j )
    {
      // k megy az oszlopokon

      for ( int k = 0; k < szelesseg; ++k )
        {

          // c = (reC, imC) a halo racspontjainak
          // megfelelo komplex szam

          reC = a + k * dx;
          imC = d - j * dy;
          std::complex<double> c ( reC, imC );

          std::complex<double> z_n ( 0, 0 );
          iteracio = 0;

          while ( std::abs ( z_n ) < 4 && iteracio < iteraciosHatar )
            {
              z_n = z_n * z_n + c;

              ++iteracio;
            }

          kep.set_pixel ( k, j,
                          png::rgb_pixel ( iteracio%255, (iteracio*iteracio)%255, 0 ) );
        }

      int szazalek = ( double ) j / ( double ) magassag * 100.0;
      std::cout << "\r" << szazalek << "%" << std::flush;
    }

  kep.write ( argv[1] );
  std::cout << "\r" << argv[1] << " mentve." << std::endl;

}
]]>
        </programlisting>
        <para>
            Ennek a verziónak a futtatása már kissé bonyolultabb:
            <programlisting>
<![CDATA[./3.1.2 mandel 1920 1080 1020 0.4126 0.4143 0.2167 0.2156]]>
            </programlisting>
        </para>
        <para>
            A program hasonlít az előzőre, de azért vannak különbségek.
            Ott a komplex számokat két változóban tároltuk, itt viszont csak egyben fogjuk.
            Ehhez a <function>complex</function> könyvtárat fogjuk használni.
        </para>
        <para>
            Megjelenik kettő, eddig nem használt függvény, az <function>atoi</function> és az <function>atof</function>.
            Ezek közül az előbbi 'string'-et konvertál át 'integer'-ré, az utóbbi pedig 'string'-et 'float'-tá.
        </para>
        <para>
            Itt is létrejön egy üres png, amin a két, egymásba ágyazott 'for' ciklus fog végigmenni, és beállítani a pixelek megfelelő értékeit.
        </para>
        <para>
            Ebben a megvalósításban használjuk a complex típust, ami ugye két részből áll: a valós és imaginárius / képzetes részből.
            Ha elérjük az iterációs határt, akkor az adott komplex szám eleme a halmaznak.
            Ha mindennek a végére értünk, akkor a képet elmentjük a megadott néven.
        </para>
    </section>        
                
    <section>
        <title>Biomorfok</title>
        <para>
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/n1CaS9VBMYo">https://youtu.be/n1CaS9VBMYo</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/attention_raising/Biomorf/3.1.3.cpp">https://gitlab.com/ak_k/bhax/-/blob/master/attention_raising/Biomorf/3.1.3.cpp</link>
        </para>
        <para>
            A biomorfokra (a Julia halmazokat rajzoló bug-os programjával) 
            rátaláló Clifford Pickover azt hitte természeti törvényre 
            bukkant: <link xlink:href="https://www.emis.de/journals/TJNSA/includes/files/articles/Vol9_Iss5_2305--2315_Biomorphs_via_modified_iterations.pdf">https://www.emis.de/journals/TJNSA/includes/files/articles/Vol9_Iss5_2305--2315_Biomorphs_via_modified_iterations.pdf</link> (lásd a 2307. oldal aljától).
        </para>       
        <para>
            A különbség a <link xlink:href="#bhax-textbook-feladatok-mandelbrot.Mandelbrot">Mandelbrot halmaz</link>
            és a Julia halmazok között az, hogy a komplex iterációban az előbbiben a c változó, utóbbiban pedig állandó. 
            A következő Mandelbrot csipet azt mutatja, hogy a c befutja a vizsgált összes rácspontot.
        </para>       
        <programlisting language="c++">
<![CDATA[  // j megy a sorokon
  for ( int j = 0; j < magassag; ++j )
    {
      for ( int k = 0; k < szelesseg; ++k )
        {

          // c = (reC, imC) a halo racspontjainak
          // megfelelo komplex szam

          reC = a + k * dx;
          imC = d - j * dy;
          std::complex<double> c ( reC, imC );

          std::complex<double> z_n ( 0, 0 );
          iteracio = 0;

          while ( std::abs ( z_n ) < 4 && iteracio < iteraciosHatar )
            {
              z_n = z_n * z_n + c;

              ++iteracio;
            }
]]>
        </programlisting>        
        <para>
            Ezzel szemben a Julia halmazos csipetben a cc nem változik, hanem minden vizsgált
            z rácspontra ugyanaz.
        </para>
        <programlisting language="c++">
<![CDATA[    // j megy a sorokon
    for ( int j = 0; j < magassag; ++j )
    {
        // k megy az oszlopokon
        for ( int k = 0; k < szelesseg; ++k )
        {
            double reZ = a + k * dx;
            double imZ = d - j * dy;
            std::complex<double> z_n ( reZ, imZ );

            int iteracio = 0;
            for (int i=0; i < iteraciosHatar; ++i)
            {
                z_n = std::pow(z_n, 3) + cc;
                if(std::real ( z_n ) > R || std::imag ( z_n ) > R)
                {
                    iteracio = i;
                    break;
                }
            }
]]>
        </programlisting>                         
        
        <para>
            A bimorfos algoritmus pontos megismeréséhez ezt a cikket javasoljuk: 
            <link xlink:href="https://www.emis.de/journals/TJNSA/includes/files/articles/Vol9_Iss5_2305--2315_Biomorphs_via_modified_iterations.pdf">https://www.emis.de/journals/TJNSA/includes/files/articles/Vol9_Iss5_2305--2315_Biomorphs_via_modified_iterations.pdf</link>.
            Az is jó gyakorlat, ha magából ebből a cikkből from scratch kódoljuk be a sajátunkat, de mi a királyi úton járva a 
            korábbi <link xlink:href="#bhax-textbook-feladatok-mandelbrot.Mandelbrot">Mandelbrot halmazt</link> kiszámoló 
            forrásunkat módosítjuk. Viszont a program változóinak elnevezését összhangba hozzuk a közlemény jelöléseivel:
        </para>       
        <programlisting language="c++">
<![CDATA[// Verzio: 3.1.3.cpp
// Forditas:
// g++ 3.1.3.cpp -lpng -O3 -o 3.1.3
// Futtatas:
// ./3.1.3 bmorf.png 800 800 10 -2 2 -2 2 .285 0 10
// Nyomtatas:
// a2ps 3.1.3.cpp -o 3.1.3.cpp.pdf -1 --line-numbers=1  --left-footer="BATF41 HAXOR STR34M" --right-footer="https://bhaxor.blog.hu/" --pro=color
// 
// BHAX Biomorphs
// Copyright (C) 2019
// Norbert Batfai, batfai.norbert@inf.unideb.hu
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Version history
//
// https://youtu.be/IJMbgRzY76E
// See also https://www.emis.de/journals/TJNSA/includes/files/articles/Vol9_Iss5_2305--2315_Biomorphs_via_modified_iterations.pdf
//

#include <iostream>
#include "png++/png.hpp"
#include <complex>

int
main ( int argc, char *argv[] )
{

    int szelesseg = 1920;
    int magassag = 1080;
    int iteraciosHatar = 255;
    double xmin = -1.9;
    double xmax = 0.7;
    double ymin = -1.3;
    double ymax = 1.3;
    double reC = .285, imC = 0;
    double R = 10.0;

    if ( argc == 12 )
    {
        szelesseg = atoi ( argv[2] );
        magassag =  atoi ( argv[3] );
        iteraciosHatar =  atoi ( argv[4] );
        xmin = atof ( argv[5] );
        xmax = atof ( argv[6] );
        ymin = atof ( argv[7] );
        ymax = atof ( argv[8] );
        reC = atof ( argv[9] );
        imC = atof ( argv[10] );
        R = atof ( argv[11] );

    }
    else
    {
        std::cout << "Hasznalat: ./3.1.2 fajlnev szelesseg magassag n a b c d reC imC R" << std::endl;
        return -1;
    }

    png::image < png::rgb_pixel > kep ( szelesseg, magassag );

    double dx = ( xmax - xmin ) / szelesseg;
    double dy = ( ymax - ymin ) / magassag;

    std::complex<double> cc ( reC, imC );

    std::cout << "Szamitas\n";

    // j megy a sorokon
    for ( int y = 0; y < magassag; ++y )
    {
        // k megy az oszlopokon

        for ( int x = 0; x < szelesseg; ++x )
        {

            double reZ = xmin + x * dx;
            double imZ = ymax - y * dy;
            std::complex<double> z_n ( reZ, imZ );

            int iteracio = 0;
            for (int i=0; i < iteraciosHatar; ++i)
            {

                z_n = std::pow(z_n, 3) + cc;
                //z_n = std::pow(z_n, 2) + std::sin(z_n) + cc;
                if(std::real ( z_n ) > R || std::imag ( z_n ) > R)
                {
                    iteracio = i;
                    break;
                }
            }

            kep.set_pixel ( x, y,
                            png::rgb_pixel ( (iteracio*20)%255, (iteracio*40)%255, (iteracio*60)%255 ));
        }

        int szazalek = ( double ) y / ( double ) magassag * 100.0;
        std::cout << "\r" << szazalek << "%" << std::flush;
    }

    kep.write ( argv[1] );
    std::cout << "\r" << argv[1] << " mentve." << std::endl;

}
]]>
        </programlisting>                                 
        <para>
            Ez is hasonlít a Mandelbrot halmazos feladathoz, viszont itt valójában Julia halmazzal dolgozunk, mivel a 'cc' értékét a felhasználó adja meg, ráadásul itt az egy konstans.
        </para>
        <para>
            Itt is létrejön egy üres png, ami majd a halmazt fogja ábrázolni.
            Megadjuk a rácsok közötti lépésközt.
            Itt is van két, egymásba ágyazott 'for' ciklusunk, amivel szintén végigmegyünk a rácson, viszont itt van még egy 'for' ciklus.
            Ezzel függvényértékeket számolunk addig, amíg el nem érjük az iterációs határt, vagy nem teljesül egy bizonyos feltétel:
            <programlisting language="c++">
<![CDATA[if (std::real (z_n) > R || std::imag (z_n) > R)]]>
            </programlisting>
        </para>              
        <para>
            A programot hasonlóan fordítjuk és futtatjuk, mint korábban:
            <programlisting>
<![CDATA[g++ 3.1.3.cpp -o biomorf -lpng
./biomorf biomorf.png 800 800 15 -6 4 -3 2 .547 2 9]]>
            </programlisting>
        </para>
        <mediaobject>
            <imageobject>
                <imagedata fileref="img/biomorf.png" format="png" scale="25"/>
            </imageobject>
            <caption>
                <para>
                    Példa biomorfra.
                </para>
            </caption>
        </mediaobject>            
    </section>                     

    <section>
        <title>A Mandelbrot halmaz CUDA megvalósítása</title>
        <para>
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/GhzeUTq9NOc">https://youtu.be/GhzeUTq9NOc</link>
        </para>
        <para>
            Megoldás forrása:                
 <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/attention_raising/CUDA/mandelpngc_60x60_100.cu">
                https://gitlab.com/ak_k/bhax/-/blob/master/attention_raising/CUDA/mandelpngc_60x60_100.cu
            </link>         
        </para>
        <para>
            Tutoráltam: Udvari Laura
        </para>
        <para>
            Ez gyakorlatilag ugyanaz, mint a fejezet első feladatában bemutatott Mandelbrot halmaz megvalósítás, csak most CUDA techonlógiát használva.
            Ez lehetővég teszi, hogy a számítást a CPU helyett a GPU hajtsa végre, amihez szükség lesz egy Nvidia videókártyára, valamint a CUDA fejlesztői környezetére.
        </para>
        <para>
            A program maga úgy működik, hogy létrehoz egy 600x600-as rácsot, amit blokkokra bont.
            Egy blokhoz tartozik egy thread, amit a GPU futtat.
        </para>
        <para>
            A forrás elején definiálva van a méret, valamint az iterációs határ értéke. Ezután a <function>mandel</function> függvénnyel létrehozzuk a halmazt.
        </para>
        <para>
            A fordítás során a fordító két részre bontja a programot: egy host, valamint egy eszközhöz kapcsolódó részre.
            Ahol oda van írva a <function>__device__</function>, vagy a <function>__global__</function> kifejezés, azokkal a részekkel fog foglalkozni az nvcc.
            Van még egy <function>threadIdx.x/y</function> kifejezés is; ez azt fogja meghatározni, hogy az aktuális x és y számítása melyik szálon fut.
        </para>
        <para>
            A <function>cudamandel</function> függvény kap egy 600x600-as tömböt, mint paramétert.
            Létrehozunk egy pointert, ami majd átadásra fog kerülni a <function>mandelkernel</function> függvénynek.
            A <function>main</function>-ben mérjük a futási időt, így könnyebben össze tudjuk hasonlítani a csak CPU-t használó verzióval, hogy valóban mennyivel gyorsabb a futási idő.
            A végén itt is végigmegy a pixeleken, hogy azoknak beállítsa a megfelelő színt, majd menti a képet a megadott néven.
        </para>
        <mediaobject>
            <imageobject>
                <imagedata fileref="img/c.png" format=".PNG" scale="25"/>
            </imageobject>
            <caption>
                <para>
                    Amint látni, ugyanazt az eredményt kaptuk, csak sokkal gyorsabban.
                </para>
            </caption>
        </mediaobject>
    </section>                     

    <section>
        <title>Mandelbrot nagyító és utazó C++ nyelven</title>
        <para>
            Építs GUI-t a Mandelbrot algoritmusra, lehessen egérrel nagyítani egy területet, illetve egy pontot
            egérrel kiválasztva vizualizálja onnan a komplex iteréció bejárta z<subscript>n</subscript> komplex számokat!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/yjNuV_5uLVQ">https://youtu.be/yjNuV_5uLVQ</link>       
            </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/ak_k/bhax/-/tree/master/attention_raising/Mandelbrot/mandel_mento">https://gitlab.com/ak_k/bhax/-/tree/master/attention_raising/Mandelbrot/mandel_mento</link>
        </para>
        <para>
            A program fordítása és futtatása után a következő képeket kapjuk:
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/1.png" format=".PNG" scale="35"/>
                </imageobject>
                <caption>
                    <para>
                        1
                    </para>
                </caption>
            </mediaobject>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/2.png" format=".PNG" scale="35"/>
                </imageobject>
                <caption>
                    <para>
                        2
                    </para>
                </caption>
            </mediaobject>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/3.png" format=".PNG" scale="35"/>
                </imageobject>
                <caption>
                    <para>
                        3
                    </para>
                </caption>
            </mediaobject>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/4.png" format=".PNG" scale="35"/>
                </imageobject>
                <caption>
                    <para>
                        4
                    </para>
                </caption>
            </mediaobject>
        </para>
    </section>                     
                                                                                                                                                                            
    <section>
        <title>Mandelbrot nagyító és utazó Java nyelven</title>
        <para>
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/uW8jAb63GFQ">https://youtu.be/uW8jAb63GFQ</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/ak_k/bhax/-/tree/master/attention_raising/Mandelbrot/java">https://gitlab.com/ak_k/bhax/-/tree/master/attention_raising/Mandelbrot/java</link>
        </para>
        <para>
            A program fordításához három állományra van szükség:
            <programlisting>
<![CDATA[MandelbrotHalmaz.java
MandelbrotHalmazNagyító.java
MandelbrotIterációk.java]]>
            </programlisting>
            Ezek közül mindháromnak meg kell lennie, mert különben nem fog lefordulni a program, hiszen a <function>MandelbrotHalmazNagyító</function>-ban hivatkozunk a másik két forrásban lévő dolgokra.
        </para>
        <para>
            Ha lefordult a program, akkor futtathatjuk is.
            Azt lehet észrevenni, hogy ugyanazt a képet kaptuk a halmazról, vizont itt már a bal egérgombbal bele tudunk nagyítani a képbe.
            Ekkor egy új ablakban megjeleníti a kinagyított részt, amibe szintén belenagyíthatunk, és így tovább...
            Az 's' billentyű lenyomásával screenshotot is készíthetünk az aktuális képről.
        </para>
        
        <mediaobject>
            <imageobject>
                <imagedata fileref="img/j2.png" format="PNG" scale="40"/>
            </imageobject>
            <caption>
                <para>
                    A teljes kép.
                </para>
            </caption>
        </mediaobject>
        <mediaobject>
            <imageobject>
                <imagedata fileref="img/j1.png" format="PNG" scale="40"/>
            </imageobject>
            <caption>
                <para>
                    Nagyítás 1.
                </para>
            </caption>
        </mediaobject>
        <mediaobject>
            <imageobject>
                <imagedata fileref="img/j3.png" format="PNG" scale="40"/>
            </imageobject>
            <caption>
                <para>
                    Nagyítás 2.
                </para>
            </caption>
        </mediaobject>
    </section>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
