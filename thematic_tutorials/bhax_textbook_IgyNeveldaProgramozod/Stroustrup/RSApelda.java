import java.lang.Math.*;
import java.math.BigInteger;
import java.util.*;

//import jdk.internal.org.objectweb.asm.ByteVector;

class KulcsPar
{
    BigInteger d, e, m;

    public KulcsPar()
    {
        int meretBitekben = 700 * (int)(Math.log((double) 10) / Math.log((double) 2));
        BigInteger p = new BigInteger(meretBitekben, 100, new Random());
        BigInteger q = new BigInteger(meretBitekben, 100, new Random());

        m = p.multiply(q);

        BigInteger z = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));

        do
        {
            do
            {
                d = new BigInteger(meretBitekben, new Random());
            }
            while (d.equals(BigInteger.ONE));
        }
        while (!z.gcd(d).equals(BigInteger.ONE));

        e = d.modInverse(z);
    }
}

public class RSApelda
{
    public static void main(String[] args)
    {
        KulcsPar jSzereplo = new KulcsPar();
        String tisztaSzoveg = "asbesthos";

        byte[] buffer = tisztaSzoveg.getBytes();
        BigInteger[] titkos = new BigInteger[buffer.length];

        for (int i = 0; i < titkos.length; ++i)
        {
            titkos[i] = new BigInteger(new byte[]{buffer[i]});
            titkos[i] = titkos[i].modPow(jSzereplo.e, jSzereplo.m);     
        }

        for (int i = 0; i < titkos.length; ++i)
        {
            buffer[i] = titkos[i].byteValue();
            titkos[i] = titkos[i].modPow(jSzereplo.d, jSzereplo.m);
        }

        System.out.println(new String(buffer));
    }
}