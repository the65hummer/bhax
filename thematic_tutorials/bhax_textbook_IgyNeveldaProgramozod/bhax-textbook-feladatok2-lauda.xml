<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Lauda!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>Port scan</title>
        <para>
            Mutassunk rá ebben a port szkennelő forrásban a kivételkezelés szerepére!
        </para>
        <para>
            A példaprogramunk egy egyszerű alkalmazás, ami ellenőrzi, hogy egy adott port nyitva van-e a hálózaton.
            Ez azért lehet hasznos, mivel így fel tudjuk tárni a hálózaton az esetleges biztonsági réseket, ahol nem kívánatos illetők megpróbálhatnak behatolni.
        </para>
        <para>
            Ebben a feladatban maga port vizsgálata történik kivételkezeléssel.
            Egy <function>while</function> ciklussal végigmegyünk az összes porton, közben egy try-catch blokkban vizsgáljuk, hogy az a port nyitva van-e, vagy sem.
            A <function>try</function> ágban megpróbáljuk példányosítani a <function>Socket</function> osztályt az adott portra, amit nézünk, aztán ha ez sikerül, akkor elmondhatjuk, hogy a port nyitva van.
            Ellenkező esetben, ha ez a példányosítás nem képes lezajlani, akkor az adott port nincs nyitva.
        </para>
        <para>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/portscan.png" format="PNG"/>
                </imageobject>
            </mediaobject>
        </para>
        
        <programlisting language="Java">
<![CDATA[import java.net.Socket;
import java.io.IOException;

public class PortScanner
{
	public static void main(String[] args)
	{
		if (args.length < 1)
		{
			System.err.println("Usage: java PortScanner <ip>");
			System.exit(-1);
		}

		Socket scannerSocket = null;
		int currentPort = 0;

		while(currentPort <= 1024)
		{
			try
			{
				scannerSocket = new Socket(args[0], currentPort);
				System.out.printf("!! %d is open!!\n", currentPort);
			}
			catch(IOException e)
			{
				System.err.printf("!! %d is not open!!\n", currentPort);
			}

			currentPort++;
		}
	}
}]]>
        </programlisting>
    </section>
    <section>
        <title>EPAM: Kivételkezelés</title>
        <para>
            Adott az alábbi kódrészlet. 
            Mi történik, ha az input változó 1F, “string” vagy pedig null? 
            Meghívódik e minden esetben a finally ág? 
            Válaszod indokold!
        </para>
        <programlisting language="Java">
<![CDATA[public void test(Object input)
{
	try
	{
		System.out.println("Try!");

		if (input instanceof Float)
		{
			throw new ChildException();
		}
		else if (input instanceof String)
		{
			throw new ParentException();
		}
		else
		{
			throw new RuntimeException();
		}
	}
	catch (ChildException e)
	{
		System.out.println("Child Exception is caught!");

		if (e instanceof ParentException)
		{
			throw new ParentException();
		}
	}
	catch (ParentException e)
	{
		System.out.println("Parent Exception is caught!");
		System.exit(1);
	}
	catch (Exception e)
	{
		System.out.println("Exception is caught!");
	}
	finally
	{
		System.out.println("Finally!");
	}
}]]>
        </programlisting>
        <para>
            A fenti kódrészlet egy kivételkezelési folyamatot mutat be.
            Adott egy <function>input</function> változó, amelynek a típúsát ellenőrizzük.
            Ha <function>float</function>, akkor <function>ChildException</function>-t fog dobni, ha <function>string</function>, akkor <function>ParentException</function>-t, egyébként meg <function>RuntimeException</function>-t.
            A <function>finally</function> ág alapértelmezés szerint mindig lefut, ebben az esetben viszont, ha <function>ParentException</function>-t kapunk, akkor a program kilép a try-catch blokkból, így a <function>finally</function> ág már nem fog lefutni.
        </para>
    </section>
	<section>
		<title>Junit teszt</title>
		<para>
			A https://progpater.blog.hu/2011/03/05/labormeres_otthon_avagy_hogyan_dolgozok_fel_egy_pedatposzt kézzel számított mélységét és szórását dolgozd be egy Junit tesztbe (sztenderd védési feladat volt korábban).
		</para>
		<para>
			A feladat szerint egy JUnit tesztet kellett készíteni arra, hogy leteszteljük, hogy a fáról számított különböző értékek valóban a valóságot tükrözik.
			A megoldáshoz a binfa Java verzióját kellett felhasználni; ezt egy Maven projektbe bedolgozva készítettem hozzá tesztet.
		</para>
		<programlisting language="Java">
<![CDATA[import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LZWUnitTest {
    
    LZWBinaryTree testTree = new LZWBinaryTree();
    String input = "01111001001001000111";
    
    @BeforeAll
    public static void startTesting() 
    {
        System.out.println("Teszteljük a fát.");
       
    }
    
    @AfterAll
    public static void endTesting() 
    {
        System.out.println("Everything's all right.");
    }
    
    @Test
    public void testAverage()
    {
        for(char b : input.toCharArray())
        {
            testTree.insert(b);
        }
        
        assertEquals(2.75, testTree.getMean(), 0.0001);
        System.out.println("A várt átlag megegyezik a kapottal!");
        System.out.printf("Várt: %f Kapott: %f\n", 2.75,testTree.getMean());
    }
    @Test
    public void testDepth()
    {
        for(char b : input.toCharArray())
        {
            testTree.insert(b);
        }
        assertEquals(4 , testTree.getDepth());
        System.out.println("A várt mélység megegyezik a kapottal!");
        System.out.printf("Várt: %d Kapott: %d\n", 4,testTree.getDepth());
    }
    @Test
    public void testVariance()
    {
        for(char b : input.toCharArray())
        {
            testTree.insert(b);
        }
        assertEquals(0.9574, testTree.getVariance(), 0.0001);
        System.out.println("A várt szórás megegyezik a kapottal!");
        System.out.printf("Várt: %f Kapott: %f\n", 0.9574,testTree.getVariance());
    }
}
]]>
		</programlisting>
		<para>
			Amint látni, három tesztet fog végezni, megadva az átlagot, a mélységet, valamint a szórást.
			Először is példányosítjuk a fa egy új példányát, majd megadunk egy teszt inputot, string formájában.
			A tesztek előtt és után kommunikál a program a felhasználóval, ezt jelzik a <function>@BeforeAll</function> és <function>@AfterAll</function> annotációk.
			Minden tesztnek az elején felfüzzük a fára a megadott inputot, amit ehhez először egy char tömbbé konvertálunk, majd elemenként felfüzzük a fára.
			Az <function>assertEquals</function> fügvénnyel fogjuk ellenőrizni, hogy jól működnek-e a dolgok.
			Megadjuk neki a várt értéket, majd pedig lefutattjuk a a teszt fára a tesztelni kívánt függvényt.
			Ha egyezik a kapott érték a várt értékkel, akkor sikeresen lefutott a teszt, a program jól működik.
		</para>
		<para>
			<mediaobject>
				<imageobject>
					<imagedata fileref="img/junit.png" format="PNG"/>
				</imageobject>
			</mediaobject>
		</para>
	</section>
    <section>
        <title>Android Játék</title>
        <para>
            Írjunk egy egyszerű Androidos „játékot”!
        </para>
        <para>
            <mediaobject>
				<imageobject>
					<imagedata fileref="img/game1.png" format="PNG" scale="25"/>
				</imageobject>
			</mediaobject>
            <mediaobject>
				<imageobject>
					<imagedata fileref="img/game2.png" format="PNG" scale="25"/>
				</imageobject>
			</mediaobject>
        </para>
        <programlisting language="java">
<![CDATA[package com.example.breakout;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

public class BreakoutGame extends Activity
{
    BreakoutView breakoutView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize gameView and set it as the view
        breakoutView = new BreakoutView(this);
        setContentView(breakoutView);

    }

    class BreakoutView extends SurfaceView implements Runnable
    {
        Thread gameThread = null;

        SurfaceHolder ourHolder;

        volatile boolean playing;

        boolean paused = true;

        Canvas canvas;
        Paint paint;

        long fps;

        private long timeThisFrame;

        int screenX;
        int screenY;

        Paddle paddle;
        Ball ball;

        Brick[] bricks = new Brick[200];
        int numBricks = 0;

        SoundPool soundPool;
        int beep1ID = -1;
        int beep2ID = -1;
        int beep3ID = -1;
        int loseLifeID = -1;
        int explodeID = -1;

        int score = 0;

        int lives = 3;

        public BreakoutView(Context context)
        {
            super(context);

            ourHolder = getHolder();
            paint = new Paint();

            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            screenX = size.x;
            screenY = size.y;

            paddle = new Paddle(screenX, screenY - 200);
            ball = new Ball(screenX, screenY);

            soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC,0);

            try
            {
                AssetManager assetManager = context.getAssets();
                AssetFileDescriptor descriptor;

                descriptor = assetManager.openFd("beep1.ogg");
                beep1ID = soundPool.load(descriptor, 0);

                descriptor = assetManager.openFd("beep2.ogg");
                beep2ID = soundPool.load(descriptor, 0);

                descriptor = assetManager.openFd("beep3.ogg");
                beep3ID = soundPool.load(descriptor, 0);

                descriptor = assetManager.openFd("loseLife.ogg");
                loseLifeID = soundPool.load(descriptor, 0);

                descriptor = assetManager.openFd("explode.ogg");
                explodeID = soundPool.load(descriptor, 0);

            }
            catch(IOException e)
            {
                Log.e("error", "failed to load sound files");
            }

            createBricksAndRestart();
        }

        public void createBricksAndRestart()
        {
            ball.reset(screenX, screenY);

            int brickWidth = screenX / 8;
            int brickHeight = screenY / 10;

            numBricks = 0;

            for(int column = 0; column < 8; column++)
            {
                for(int row = 0; row < 3; row++)
                {
                    bricks[numBricks] = new Brick(row, column, brickWidth, brickHeight);
                    numBricks++;
                }
            }

            score = 0;
            lives = 3;
        }

        @Override
        public void run()
        {
            while (playing)
            {
                long startFrameTime = System.currentTimeMillis();

                if(!paused)
                {
                    update();
                }

                draw();

                timeThisFrame = System.currentTimeMillis() - startFrameTime;
                if (timeThisFrame >= 1) {
                    fps = 1000 / timeThisFrame;
                }
            }
        }

        public void update()
        {
            paddle.update(fps);
            ball.update(fps);

            for(int i = 0; i < numBricks; i++)
            {
                if (bricks[i].getVisibility())
                {
                    if(RectF.intersects(bricks[i].getRect(),ball.getRect()))
                    {
                        bricks[i].setInvisible();
                        ball.reverseYVelocity();
                        score = score + 10;
                        soundPool.play(explodeID, 1, 1, 0, 0, 1);
                    }
                }
            }

            if(RectF.intersects(paddle.getRect(),ball.getRect()))
            {
                ball.setRandomXVelocity();
                ball.reverseYVelocity();
                ball.clearObstacleY(paddle.getRect().top - 2);
                soundPool.play(beep1ID, 1, 1, 0, 0, 1);
            }

            if(ball.getRect().bottom > screenY)
            {
                ball.reverseYVelocity();
                ball.clearObstacleY(screenY - 2);

                lives --;
                soundPool.play(loseLifeID, 1, 1, 0, 0, 1);

                if(lives == 0)
                {
                    paused = true;
                    createBricksAndRestart();
                }
            }

            if(ball.getRect().top < 0)
            {
                ball.reverseYVelocity();
                ball.clearObstacleY(12);
                soundPool.play(beep2ID, 1, 1, 0, 0, 1);
            }

            if(ball.getRect().left < 0)
            {
                ball.reverseXVelocity();
                ball.clearObstacleX(2);
                soundPool.play(beep3ID, 1, 1, 0, 0, 1);
            }

            if(ball.getRect().right > screenX - 10)
            {
                ball.reverseXVelocity();
                ball.clearObstacleX(screenX - 22);
                soundPool.play(beep3ID, 1, 1, 0, 0, 1);
            }

            if(score == numBricks * 10)
            {
                paused = true;
                createBricksAndRestart();
            }
        }

        public void draw()
        {
            if (ourHolder.getSurface().isValid())
            {
                canvas = ourHolder.lockCanvas();

                canvas.drawColor(Color.argb(255,  26, 128, 182));

                paint.setColor(Color.argb(255,  255, 255, 255));

                canvas.drawRect(paddle.getRect(), paint);
                canvas.drawRect(ball.getRect(), paint);

                paint.setColor(Color.argb(255,  249, 129, 0));

                for(int i = 0; i < numBricks; i++)
                {
                    if(bricks[i].getVisibility())
                    {
                        canvas.drawRect(bricks[i].getRect(), paint);
                    }
                }

                paint.setColor(Color.argb(255,  255, 255, 255));

                paint.setTextSize(40);
                canvas.drawText("Score: " + score + "   Lives: " + lives, 10,50, paint);

                if(score == numBricks * 10)
                {
                    paint.setTextSize(90);
                    canvas.drawText("YOU HAVE WON!", 10,screenY/2, paint);
                }

                if(lives <= 0)
                {
                    paint.setTextSize(90);
                    canvas.drawText("YOU HAVE LOST!", 10,screenY/2, paint);
                }

                ourHolder.unlockCanvasAndPost(canvas);
            }
        }

        public void pause()
        {
            playing = false;
            try
            {
                gameThread.join();
            }
            catch (InterruptedException e)
            {
                Log.e("Error:", "joining thread");
            }
        }

        public void resume()
        {
            playing = true;
            gameThread = new Thread(this);
            gameThread.start();
        }

        @Override
        public boolean onTouchEvent(MotionEvent motionEvent)
        {
            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK)
            {
                case MotionEvent.ACTION_DOWN:
                    paused = false;

                    if (motionEvent.getX() > screenX / 2)
                    {
                        paddle.setMovementState(paddle.RIGHT);
                    }
                    else
                    {
                        paddle.setMovementState(paddle.LEFT);
                    }

                    break;

                case MotionEvent.ACTION_UP:
                    paddle.setMovementState(paddle.STOPPED);
                    break;
            }
            return true;
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        breakoutView.resume();
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        breakoutView.pause();
    }
}]]>
        </programlisting>
        <programlisting language="java">
<![CDATA[package com.example.breakout;

import android.graphics.RectF;

public class Brick
{
    private RectF rect;

    private boolean isVisible;

    public Brick(int row, int column, int width, int height)
    {
        isVisible = true;

        int padding = 1;

        rect = new RectF(column * width + padding,
                row * height + padding,
                column * width + width - padding,
                row * height + height - padding);
    }

    public RectF getRect()
    {
        return this.rect;
    }

    public void setInvisible()
    {
        isVisible = false;
    }

    public boolean getVisibility()
    {
        return isVisible;
    }
}]]>
        </programlisting>
        <programlisting language="java">
<![CDATA[package com.example.breakout;

import android.graphics.RectF;

public class Paddle
{
    private RectF rect;

    private float length;
    private float height;

    private float x;

    private float y;

    private float paddleSpeed;

    public final int STOPPED = 0;
    public final int LEFT = 1;
    public final int RIGHT = 2;

    private int paddleMoving = STOPPED;

    public Paddle(int screenX, int screenY)
    {
        length = 130;
        height = 20;

        x = screenX / 2;
        y = screenY - 20;

        rect = new RectF(x, y, x + length, y + height);

        paddleSpeed = 350;
    }

    public RectF getRect()
    {
        return rect;
    }

    public void setMovementState(int state)
    {
        paddleMoving = state;
    }

    public void update(long fps)
    {
        if(paddleMoving == LEFT)
        {
            x = x - paddleSpeed / fps;
        }

        if(paddleMoving == RIGHT)
        {
            x = x + paddleSpeed / fps;
        }

        rect.left = x;
        rect.right = x + length;
    }
}]]>
        </programlisting>
        <programlisting language="java">
<![CDATA[package com.example.breakout;

import android.graphics.RectF;

import java.util.Random;

public class Ball
{
    RectF rect;
    float xVelocity;
    float yVelocity;
    float ballWidth = 10;
    float ballHeight = 10;

    public Ball(int screenX, int screenY)
    {
        xVelocity = 200;
        yVelocity = -400;

        rect = new RectF();
    }

    public RectF getRect()
    {
        return rect;
    }

    public void update(long fps)
    {
        rect.left = rect.left + (xVelocity / fps);
        rect.top = rect.top + (yVelocity / fps);
        rect.right = rect.left + ballWidth;
        rect.bottom = rect.top - ballHeight;
    }

    public void reverseYVelocity()
    {
        yVelocity = -yVelocity;
    }

    public void reverseXVelocity()
    {
        xVelocity = - xVelocity;
    }

    public void setRandomXVelocity()
    {
        Random generator = new Random();
        int answer = generator.nextInt(2);

        if(answer == 0)
        {
            reverseXVelocity();
        }
    }

    public void clearObstacleY(float y)
    {
        rect.bottom = y;
        rect.top = y - ballHeight;
    }

    public void clearObstacleX(float x)
    {
        rect.left = x;
        rect.right = x + ballWidth;
    }

    public void reset(int x, int y)
    {
        rect.left = x / 2;
        rect.top = y - 20;
        rect.right = x / 2 + ballWidth;
        rect.bottom = y - 20 - ballHeight;
    }
}]]>
        </programlisting>
    </section>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
</chapter>                