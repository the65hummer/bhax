<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Turing!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Végtelen ciklus</title>
        <para>
            Írj olyan C végtelen ciklusokat, amelyek 0 illetve 100 százalékban dolgoztatnak egy magot és egy olyat, amely  
            100 százalékban minden magot!
        </para>
        <para>
            Megoldás videó:
            <link xlink:href="https://youtu.be/b3YpXl4QCvY">https://youtu.be/b3YpXl4QCvY</link>
        </para>
        <para>
            Megoldás forrása: 
            <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/vegtelen1.c">
                https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/vegtelen1.c
            </link>, 
            <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/vegtelen2.c">
                https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/vegtelen2.c
            </link>.
        </para>
        <para>
            Végtelen ciklust többféleképpen is hozhatunk létre. Egy végtelen ciklus létrejötte lehet saját, szándékos munkánk, viszont egy nem megfelelően kezelt ciklusváltozó is lehet a ludas.
        </para>                    
        <para>
            Egy mag 100 százalékban:               
        </para>
        <programlisting language="c">
            <![CDATA[
    int
    main ()
    {
      for (;;)

      return 0;
    }
            ]]>
        </programlisting>        
        <para>        
        ez a változat talán átláthatóbb, viszont kevésbé hordozható különböző fodítók között:
        </para>
        <programlisting language="c"><![CDATA[
    #include <stdbool.h>
    int
    main ()
    {
      while(true);
    
      return 0;
    }
            ]]>
        </programlisting>        
        <para>
            Érdemesebb a <literal>for(;;)</literal> formát hasznáni, mivel így mások által könnyebben felismerhető, hogy ez a végtelen ciklus szándékos volt, és nem programhiba eredménye.
        </para>            
        <para>
            A fordító mindkét formát ugyanarra a gépi kódra fordítja:
        </para>            
        <screen><![CDATA[$ gcc -S -o infty-f.S infty-f.c 
            $ gcc -S -o infty-w.S infty-w.c 
            $ diff infty-w.S infty-f.S 
            1c1
            < 	.file	"infty-w.c"
            ---
            > 	.file	"infty-f.c"
            ]]>
        </screen>  
        <para>
            Egy mag 0 százalékban:               
        </para>        
        <programlisting language="c"><![CDATA[
    #include <unistd.h>
    int
    main ()
    {
      for (;;)
        sleep(1);
        
      return 0;
    }
            ]]>
        </programlisting>        
        <para>
            Minden mag 100 százalékban:               
        </para>
        <programlisting language="c"><![CDATA[
    #include <omp.h>
    int
    main ()
    {
    #pragma omp parallel
    {
      for (;;);
    }
      return 0;
    }
            ]]>
        </programlisting>        
        <para>
            Fordítás után, ügyelve arra, hogy valóban megtörténjen a multi-threading az -fopenmp kapcsolóval, a top parancsot kiadva, vagy a System Monitor-ral láthatjuk, hogy valóban az összes magot terheli a program. 
        </para>
    </section>
    <section>
        <title>Lefagyott, nem fagyott, akkor most mi van?</title>
        <para>
            Mutasd meg, hogy nem lehet olyan programot írni, amely bármely más programról eldönti, hogy le fog-e fagyni vagy sem!
        </para>
        <para>
            Megoldás videó: N/A
        </para>
        <para>
            Megoldás forrása:  tegyük fel, hogy akkora haxorok vagyunk, hogy meg tudjuk írni a <function>Lefagy</function>
            függvényt, amely tetszőleges programról el tudja dönteni, hogy van-e benne vlgtelen ciklus:              
        </para>
        <programlisting language="c"><![CDATA[Program T100
            {
            
            	boolean Lefagy(Program P)
            	{
            		 if(P-ben van végtelen ciklus)
            			return true;
            		 else
            			return false; 
            	}

            	main(Input Q)
            	{
            		Lefagy(Q)
            	}
            }]]>
        </programlisting>            
        <para>
            A program futtatása, például akár az előző <filename>v.c</filename> ilyen pszeudókódjára:
            <screen><![CDATA[T100(t.c.pseudo)
            true]]></screen>            
            akár önmagára
            <screen><![CDATA[T100(T100)
            false]]></screen>  
            ezt a kimenetet adja.          
        </para>
        <para>
            A T100-as programot felhasználva készítsük most el az alábbi T1000-set, amelyben a
            Lefagy-ra épőlő Lefagy2 már nem tartalmaz feltételezett, csak csak konkrét kódot:
        </para>
        <programlisting language="c"><![CDATA[Program T1000
            {
            
            	boolean Lefagy(Program P)
            	{
            		 if(P-ben van végtelen ciklus)
            			return true;
            		 else
            			return false; 
            	}

            	boolean Lefagy2(Program P)
            	{
            		 if(Lefagy(P))
            			return true;
            		 else
            			for(;;); 
            	}

            	main(Input Q)
            	{
            		Lefagy2(Q)
            	}

            }]]>
        </programlisting>            
        <programlisting><![CDATA[]]></programlisting>            
        <para>
            Mit for kiírni erre a <computeroutput>T1000(T1000)</computeroutput> futtatásra?
                                
            <itemizedlist>
                <listitem>
                    <para>Ha T1000 lefagyó, akkor nem fog lefagyni, kiírja, hogy true</para>                        
                </listitem>
                <listitem>
                    <para>Ha T1000 nem fagyó, akkor pedig le fog fagyni...</para>                        
                </listitem>
            </itemizedlist>
            akkor most hogy fog működni? Sehogy, mert ilyen <function>Lefagy</function>
            függvényt, azaz a T100 program nem is létezik.                
        </para>
        <para>
            
        </para>
    </section>            
    <section>
        <title>Változók értékének felcserélése</title>
        <para>
            Írj olyan C programot, amely felcseréli két változó értékét, bármiféle logikai utasítás vagy kifejezés
            nasználata nélkül!
        </para>
        <para>
            Megoldás videó: 
            <link xlink:href="https://youtu.be/ldtETeCw_kg">https://youtu.be/ldtETeCw_kg</link>
        </para>
        <para>
            Megoldás forrása:
            <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/csere.c">
                https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/csere.c
            </link>
        </para>
        <para>
            <programlisting>
                <![CDATA[
    #include <stdio.h>

    int main()
    {
        int a = 536;
        int b = 432;
    
        printf("a = %d, b = %d\n", a, b);
    
        b = b - a;
        a = a + b;
        b = a - b;
    
        printf("a = %d, b = %d\n", a, b);
    
        return 0;
    }
                ]]>
            </programlisting>
        </para>

        <para>
            A logikai utasítás nélküli csere többféleképpen is megközelíthető. Az én példámban összeadások és kivonások szerepelnek.
            A kezdetben van ugye a két változónk, amelyek értékét fel akarjuk cserélni.
            Először is a 'b' értékét felülírjuk a (b - a) művelet eredményével, vagyis a 'b' és az 'a' különbségével.
            Ezután az 'a-hoz' hozzáadjuk ezt a különbséget, így az 'a-ban' már 'b' eredeti értéke van.
            Majd végül ebből az új 'a-ból' kivonjuk az új 'b-t', így a 'b' értéke 'a' eredeti értéke lesz.
        </para>
        
        <para>
            A példa alapján ez így néz ki, lépésről-lépésre:
        </para>
        <orderedlist numeration="arabic">
            <listitem override="0">
                <para>
                    a = 536, b = 432
                </para>
            </listitem>
            <listitem>
                <para>
                    b = b - a, vagyis b = 432 - 536, azaz -104
                </para>
            </listitem>
            <listitem>
                <para>
                    a = a + b, vagyis a = 536 + (-101), azaz 432 ('b' eredeti értéke)
                </para>
            </listitem>
            <listitem>
                <para>
                    b = a - b, vagyis b = 432 - (-104), azaz 536 ('a' eredeti értéke)
                </para>
            </listitem>
        </orderedlist>
        <!--
        <para>
            0) a = 536, b = 432
        </para>
        <para>
            1) b = b - a, vagyis b = 432 - 536, azaz -104
        </para>
        <para>
            2) a = a + b, vagyis a = 536 + (-101), azaz 432 ('b' eredeti értéke)
        </para>
        <para>
            3) b = a - b, vagyis b = 432 - (-104), azaz 536 ('a' eredeti értéke)
        </para>
        -->

        <para>
            A két változó értéké tehát fel lett cserélve logikai utasítás, vagy segédváltozó használata nélkül.
        </para>
        
    </section>                     
    <section>
        <title>Labdapattogás</title>
        <para>
            Először if-ekkel, majd bármiféle logikai utasítás vagy kifejezés
            nasználata nélkül írj egy olyan programot, ami egy labdát pattogtat a karakteres konzolon! (Hogy mit értek
            pattogtatás alatt, alább láthatod a videókon.)
        </para>
        <para>
            Megoldás videó:
            <link xlink:href="https://youtu.be/GBIWTwpW8sw">https://youtu.be/GBIWTwpW8sw</link>
        </para>
        <para>
            Megoldás forrása (if-ekkel):
            <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/pattog.c">
                https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/pattog.c
            </link>
            Megoldás forrása (if-ek nélkül):
            <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/pattog_no.c">https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/pattog_no.c</link>
        </para>
        <para>
            <programlisting language="c">
    <![CDATA[
    WINDOW *ablak;
    ]]>
            </programlisting>
            Egy Windows típusú pointer definiálása.

            <programlisting language="c">
    <![CDATA[
    ablak = initscr();
    ]]>
            </programlisting>
            A képernyő adatainak hozzárendelése a pointerhez.

            <programlisting language="c">
    <![CDATA[
    int x = 0;
    int y = 0;
    ]]>
            </programlisting>
            A labda kiinduló pozíciója, egészként tárolva.

            <programlisting language="c">
    <![CDATA[
    int xnov = 1;
    int ynov = 1;
    ]]>
            </programlisting>
            A lépésközök.

            <programlisting language="c">
    <![CDATA[
    int mx;
    int my;
    ]]>
            </programlisting>
            Jelenleg hol járunk.

            <programlisting language="c">
    <![CDATA[
    mvprint(y, x, "O");
    ]]>
            </programlisting>
            A végtelen ciklus indítása után az mvprint() függvény a képernyőre lerajzolja a labdát. Paraméterlistája tartalmazza, hogy hova rajzoljon (y, x), valamint, hogy mit is rakjon oda (ebben az esetben egy "O" betűt.)

            <programlisting language="c">
    <![CDATA[
    usleep(100000)
    ]]>
            </programlisting>
            A usleep függvény szünetelteti a program futását. Ebben az esetben ez 100000 mikroszekudnumot jelent.

            <programlisting language="c">
    <![CDATA[
    clear();
    refresh();
    ]]>
            </programlisting>
            A clear() függvény törli a képernyő tartalmát, a refresh() pedig frissíti a képernyőt.

            <programlisting language="c">
    <![CDATA[
    if (x > mx - 1)
    {
        xnov = xnov * -1;
    }
    ]]>
            </programlisting>
            Az első if azt figyeli, hogy a labda elérte-e a képernyő jobb oldalát.

            <programlisting language="c">
    <![CDATA[
    if (x <= 0)
    {
        xnov = xnov * -1;
    }
    ]]>
            </programlisting>
            A második if azt figyeli, hogy a labda elérte a képernyő bal oldalát.

            <programlisting language="c">
    <![CDATA[
    if (y <= 0)
    {
        ynov = ynov * -1;
    }
    ]]>
            </programlisting>
            A harmadik if azt figyeli, hogy a labda elérte-e a képernyő tetejét.

            <programlisting language="c">
    <![CDATA[
    if (y > my - 1)
    {
        xnov = ynov * -1;
    }
    ]]>
            </programlisting>
            A negyedik, és egyben utolsó if pedig azt figyeli, hogy a labda elérte-e a képernyő alját.

            Mint látható, a program maga egyszerű, és viszonylag jól alkalmazkodik ahhoz, ha futtatás közben átméretezzük az ablakot (egy bizonyos fokig).
        </para>
    </section>                     
    <section>
        <title>Szóhossz és a Linus Torvalds féle BogoMIPS</title>
        <para>
            Írj egy programot, ami megnézi, hogy hány bites a szó a gépeden, azaz mekkora az <type>int</type> mérete.
            Használd ugyanazt a while ciklus fejet, amit Linus Torvalds a BogoMIPS rutinjában! 
        </para>
        <para>
            Megoldás videó:
            <link xlink:href="https://youtu.be/ktfLJixQpsI">https://youtu.be/ktfLJixQpsI</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/szohossz.cpp">
                https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/szohossz.cpp
            </link>
        </para>
        <para>
            <programlisting language="c">
    <![CDATA[
    #include <iostream>
    using namespace std;

    int main()
    {
        int t = 1;
        int t1 = 0;

        while (t != 0)
        {
            t = t << 1;
            t1++;
        }

        std::cout << t1 << "\n";
    }
    ]]>
            </programlisting>
            A ciklusmag-ban egy bitshiftelés művelet hajtódik végre, egészen mindaddig, amíg az adott változó (a példa esetében a 't') csupa nullából nem fog állni. Minden iteráció során a számláló - a példában ez a 't1' változó - értékét növeljük 1-el. A program futása után láthatjuk, hogy a 't1' értéke 32, azaz ennyi bit hosszú az adott integer.
        </para>
    </section>                     
    <section>
        <title>Helló, Google!</title>
        <para>
            Írj olyan C programot, amely egy 4 honlapból álló hálózatra kiszámolja a négy lap Page-Rank 
            értékét!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/zvuV0SaIBgA">https://youtu.be/zvuV0SaIBgA</link>
        </para>
        <para>
            Megoldás forrása: 
            <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/pagerank.c">
                https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/pagerank.c
            </link>
        </para>
        <para>
            <programlisting language = "c">
<![CDATA[#include <stdio.h>

int main() 
{
    double linkmatrix[4][4] = 
    {
        {0.0,0.0,1.0/3,0.0},
        {1.0,1.0/2.0,1.0/3.0,1.0},                         
        {0.0,1.0/2.0,0.0,0.0},
        {0.0,0.0,1.0/3.0,0.0}
    };
    double elozo[4] = {1.0/4.0,1.0/4.0,1.0/4.0,1.0/4.0};   
    double ujabb[4] = {0.0,0.0,0.0,0.0};                                                         
    for (int i=0;i<10;i++)
    {                                  
        for (int j=0; j<4; j++)
        {                            
            for (int k=0; k<4; k++)
            {                        
                if (k==0)
                {
		            ujabb[j] = 0.0;
		        }                 
                if (linkmatrix[j][k] !=0.0) 
		        {               
                    ujabb[j] = ujabb[j]+(elozo[k]*linkmatrix[j][k]);      
                }
            }
        }
        for (int l=0;l<4;l++)
    	{
    		elozo[l]=ujabb[l];
    	}               
        for (int l=0; l<4;l++)
    	{
    	printf("%d\n", ujabb[l]);  
                if (l==3)
    	        {
    		        printf("-------------------------------\n");
    	        }
    	} 
    }
    return 0;
}]]>
            </programlisting>
        </para>
        <para>
            A program elején inicializálunk egy 4*4-es mátrixot, valamint még két további mátrixot: 'elozo' és 'ujabb'.
            Az előbbi az iteráció előtti értéket tartalmazza, az utóbbi pedig az adott iteráció utáni értéket.
        </para>
        <para>
            A három, egymásba ágyazott ciklus változói különbző jelentéssel bírnak: az 'i' az iteráció sorszáma, a 'j' a bemenettel rendelkező oldal, a 'k' pedig a kimenettel rendelkező oldal.
        </para>
        <para>
            A harmadik ciklus magjában történik maga a PR érték számítása. Ha végigment az egész mátrixon, akkor a kapott értékeket eltárolja, majd a képernyőre kiírja.
        </para>
    </section>                                                                                                                                                                                                                                                                                                                                          
    <section xml:id="bhax-textbook-feladatok-turing.MontyHall">
        <title>A Monty Hall probléma</title>
        <para>
            Írj R szimulációt a Monty Hall problémára!
        </para>
        <para>
            Megoldás videó: N/A
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/mh.r">https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/mh.r</link>
        </para>
        <para>
            A Monty Hall-probléma / paradoxon egy valószínűségi paradoxon, ami az Amerikai Egyesült államokból származik.
            Nevét a "Let's Make a Deal" c. televíziós vetélkedő műsorvezetőjére kapta.
            A műsor végén a játékosnak prezentálnak három ajtót, amelyek közül kettő mögött egy-egy kecske van, egy mögött pedig egy autó.
            A játékos választ egy ajtót, de nem nyitja ki egyből, hanem előbb a műsorvezető (aki tudja, melyik ajtó mögött mi van) kinyitja az egyik, kecskét rejtő ajtót.
            Ezután megkérdezi a játékostól, hogy akar-e változatni az ajtaján.
            A paradoxon jelleg itt jön elő, ugyanis a józan ész azt diktálná, hogy nem éri meg változtatni, viszont ez nem igaz.
        </para>
        <para>
            A probléma megoldása a következő:
            Amint a műsorvezető kinyitja az egyik, kecskét rejtő ajtót, a játékos nyerési eseélyei megduplázódnak.
            Az első választásnál 1/3-ad az esélye annak, hogy a kocsit választotta, 2/3-ad, hogy az egyik kecskét.
            Azzal, hogy a műsorveztő eztuán kinyitja az egyik kecskét rejtő ajtót, a valószínűségek nem változnak, viszont ezek után már csak az egyik ajtó van zárva.
        </para>
        <para>
            Tehát csak az esetek egyharmadában fordul elő, hogy a műsorvezető bármely két fennmaradó ajtót választhatja, viszont az esetek kétharmadában csak egy bizonyos ajtót választhat.
        </para>
    </section>
    <section xml:id="Brun">
        <title>100 éves a Brun tétel</title>
        <para>
            Írj R szimulációt a Brun tétel demonstrálására!
        </para>
        <para>
            Megoldás videó:
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/Primek_R">https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/Primek_R</link>
        </para>

        <para>
            A természetes számok építőelemei a prímszámok. Abban az értelemben, 
            hogy minden természetes szám előállítható prímszámok szorzataként.
            Például 12=2*2*3, vagy például 33=3*11.
        </para>
        <para>
            Prímszám az a természetes szám, amely csak önmagával és eggyel 
            osztható. Eukleidész görög matematikus már Krisztus előtt tudta, 
            hogy végtelen sok prímszám van, de ma sem tudja senki, hogy 
            végtelen sok ikerprím van-e. Két prím ikerprím, ha különbségük 2.
        </para>
        <para>
            Két egymást követő páratlan prím között a legkisebb távolság a 2, 
            a legnagyobb távolság viszont bármilyen nagy lehet! Ez utóbbit 
            könnyű bebizonyítani. Legyen n egy tetszőlegesen nagy szám. 
            Akkor szorozzuk össze n+1-ig a számokat, azaz számoljuk ki az 
            1*2*3*… *(n-1)*n*(n+1) szorzatot, aminek a neve (n+1) faktoriális, 
            jele (n+1)!.
        </para>
        <para>
            Majd vizsgáljuk meg az a sorozatot:
        </para>    
        <para>
            (n+1)!+2, (n+1)!+3,… , (n+1)!+n, (n+1)!+ (n+1) ez n db egymást követő azám, ezekre (a jól ismert
            bizonyítás szerint) rendre igaz, hogy            
        </para>    
        <itemizedlist>
            <listitem>
                <para>(n+1)!+2=1*2*3*… *(n-1)*n*(n+1)+2, azaz 2*valamennyi+2, 2 többszöröse, így ami osztható kettővel</para>
            </listitem>
            <listitem>
                <para>(n+1)!+3=1*2*3*… *(n-1)*n*(n+1)+3, azaz 3*valamennyi+3, ami osztható hárommal</para>
            </listitem>
            <listitem>
                <para>...</para>
            </listitem>
            <listitem>
                <para>(n+1)!+(n-1)=1*2*3*… *(n-1)*n*(n+1)+(n-1), azaz (n-1)*valamennyi+(n-1), ami osztható (n-1)-el</para>
            </listitem>
            <listitem>
                <para>(n+1)!+n=1*2*3*… *(n-1)*n*(n+1)+n, azaz n*valamennyi+n-, ami osztható n-el</para>
            </listitem>
            <listitem>
                <para>(n+1)!+(n+1)=1*2*3*… *(n-1)*n*(n+1)+(n-1), azaz (n+1)*valamennyi+(n+1), ami osztható (n+1)-el</para>
            </listitem>
        </itemizedlist>
        <para>
            tehát ebben a sorozatban egy prim nincs, akkor a (n+1)!+2-nél 
            kisebb első prim és a (n+1)!+ (n+1)-nél nagyobb első 
            prim között a távolság legalább n.            
        </para>    
        <para>
            Az ikerprímszám sejtés azzal foglalkozik, amikor a prímek közötti 
            távolság 2. Azt mondja, hogy az egymástól 2 távolságra lévő prímek
            végtelen sokan vannak.
        </para>    
        <para>
            A Brun tétel azt mondja, hogy az ikerprímszámok reciprokaiból képzett sor összege, azaz
            a (1/3+1/5)+ (1/5+1/7)+ (1/11+1/13)+... véges vagy végtelen sor konvergens, ami azt jelenti, hogy ezek
            a törtek összeadva egy határt adnak ki pontosan vagy azt át nem lépve növekednek, 
            ami határ számot B<subscript>2</subscript> Brun konstansnak neveznek. Tehát ez
            nem dönti el a több ezer éve nyitott kérdést, hogy az ikerprímszámok halmaza végtelen-e? 
            Hiszen ha véges sok van és ezek
            reciprokait összeadjuk, akkor ugyanúgy nem lépjük át a B<subscript>2</subscript> Brun konstans értékét, 
            mintha végtelen 
            sok lenne, de ezek már csak olyan csökkenő mértékben járulnának hozzá a végtelen sor összegéhez, 
            hogy így sem lépnék át a Brun konstans értékét.     
        </para>
        <para>
            Ebben a példában egy olyan programot készítettünk, amely közelíteni próbálja a Brun konstans értékét.
            A repó <link xlink:href="../../../bhax/attention_raising/Primek_R/stp.r">
                <filename>bhax/attention_raising/Primek_R/stp.r</filename>
            </link> mevű állománya kiszámolja az ikerprímeket, összegzi
            a reciprokaikat és vizualizálja a kapott részeredményt.
        </para>
        <programlisting language="R">
            <![CDATA[#   Copyright (C) 2019  Dr. Norbert Bátfai, nbatfai@gmail.com
            #
            #   This program is free software: you can redistribute it and/or modify
            #   it under the terms of the GNU General Public License as published by
            #   the Free Software Foundation, either version 3 of the License, or
            #   (at your option) any later version.
            #
            #   This program is distributed in the hope that it will be useful,
            #   but WITHOUT ANY WARRANTY; without even the implied warranty of
            #   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            #   GNU General Public License for more details.
            #
            #   You should have received a copy of the GNU General Public License
            #   along with this program.  If not, see <http://www.gnu.org/licenses/>

            library(matlab)

            stp <- function(x){
            
                primes = primes(x)
                diff = primes[2:length(primes)]-primes[1:length(primes)-1]
                idx = which(diff==2)
                t1primes = primes[idx]
                t2primes = primes[idx]+2
                rt1plust2 = 1/t1primes+1/t2primes
                return(sum(rt1plust2))
            }

            x=seq(13, 1000000, by=10000)
            y=sapply(x, FUN = stp)
            plot(x,y,type="b")
            ]]>
        </programlisting>        
        <para>
            Soronként értelemezzük ezt a programot:
        </para>                
        <programlisting language="R">
            <![CDATA[ primes = primes(13)]]>
        </programlisting>        
        <para>
            Kiszámolja a megadott számig a prímeket.             
        </para>
        <screen>
            <![CDATA[> primes=primes(13)
            > primes
            [1]  2  3  5  7 11 13
            ]]>
        </screen>     
        <programlisting language="R">
            <![CDATA[ diff = primes[2:length(primes)]-primes[1:length(primes)-1]]]>
        </programlisting>        
        <screen>
            <![CDATA[> diff = primes[2:length(primes)]-primes[1:length(primes)-1]
            > diff
            [1] 1 2 2 4 2
            ]]>
        </screen>        
        <para>
            Az egymást követő prímek különbségét képzi, tehát 3-2, 5-3, 7-5, 11-7, 13-11.
        </para>
        <programlisting language="R">
            <![CDATA[idx = which(diff==2)]]>
        </programlisting>        
        <screen>
            <![CDATA[> idx = which(diff==2)
            > idx
            [1] 2 3 5
            ]]>
        </screen>              
        <para>
            Megnézi a <varname>diff</varname>-ben, hogy melyiknél lett kettő az eredmény, mert azok az ikerprím párok, ahol ez igaz.
            Ez a <varname>diff</varname>-ben lévő 3-2, 5-3, 7-5, 11-7, 13-11 külünbségek közül ez a 2., 3. és 5. indexűre teljesül.
        </para>
        <programlisting language="R">
            <![CDATA[t1primes = primes[idx]]]>
        </programlisting>  
        <para>
            Kivette a primes-ból a párok első tagját. 
        </para>
        <programlisting language="R">
            <![CDATA[t2primes = primes[idx]+2]]>
        </programlisting>        
        <para>
            A párok második tagját az első tagok kettő hozzáadásával képezzük.
        </para>
        <programlisting language="R">
            <![CDATA[rt1plust2 = 1/t1primes+1/t2primes]]>
        </programlisting>        
        <para>
            Az 1/t1primes a t1primes 3,5,11 értékéből az alábbi reciprokokat képzi:
        </para>
        <screen>
            <![CDATA[> 1/t1primes
            [1] 0.33333333 0.20000000 0.09090909
            ]]>
        </screen>                      
        <para>
            Az 1/t2primes a t2primes 5,7,13 értékéből az alábbi reciprokokat képzi:
        </para>
        <screen>
            <![CDATA[> 1/t2primes
            [1] 0.20000000 0.14285714 0.07692308
            ]]>
        </screen>                      
        <para>
            Az 1/t1primes + 1/t2primes pedig ezeket a törteket rendre összeadja.
        </para>
        <screen>
            <![CDATA[> 1/t1primes+1/t2primes
            [1] 0.5333333 0.3428571 0.1678322
            ]]>
        </screen>                      
        <para>
            Nincs más dolgunk, mint ezeket a törteket összeadni a 
            <function>sum</function> függvénnyel.
        </para>
        
        <programlisting language="R">
            <![CDATA[sum(rt1plust2)]]>
        </programlisting>    
        <screen>
            <![CDATA[>   sum(rt1plust2)
            [1] 1.044023
            ]]>
        </screen>            
        <para>
            A következő ábra azt mutatja, hogy a szumma értéke, hogyan nő, egy határértékhez tart, a 
            B<subscript>2</subscript> Brun konstanshoz. Ezt ezzel a csipettel rajzoltuk ki, ahol először a fenti 
            számítást 13-ig végezzük, majd 10013, majd 20013-ig, egészen 990013-ig, azaz közel 1 millióig.
            Vegyük észre, hogy az ábra első köre, a 13 értékhez tartozó 1.044023.
        </para>
        <programlisting language="R">
            <![CDATA[x=seq(13, 1000000, by=10000)
            y=sapply(x, FUN = stp)
            plot(x,y,type="b")]]>
        </programlisting>          
        <figure>
            <title>A B<subscript>2</subscript> konstans közelítése</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/BrunKorok.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>A B<subscript>2</subscript> konstans közelítése</phrase>
                </textobject>
            </mediaobject>
        </figure>                             
    </section>
    <section>
        <title>Vörös Pipacs Pokol/csiga folytonos mozgási parancsokkal</title>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/uA6RHzXH840">https://youtu.be/uA6RHzXH840</link>      
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://github.com/nbatfai/RedFlowerHell">https://github.com/nbatfai/RedFlowerHell</link>               
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat... ezt kell az olvasónak kidolgoznia, mint labor- vagy otthoni mérési feladatot!
            Ha mi már megtettük, akkor használd azt, dolgozd fel, javítsd, adj hozzá értéket!
        </para>            
    </section>  
</chapter>